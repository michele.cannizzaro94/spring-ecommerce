package com.dslab.ecommercelab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommercelabApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommercelabApplication.class, args);
    }

}
